package AIF.AerialVehicles.Katmam.Haron;
import AIF.Entities.Coordinates;
import AIF.Gear.Sensor;
import AIF.Gear.Weapon;

public class Eitan extends Haron {
    public Eitan(Coordinates location) {
        super(location);
        this.gearsToLoad.add(Sensor.class);
        this.gearsToLoad.add(Weapon.class);
    }
}
