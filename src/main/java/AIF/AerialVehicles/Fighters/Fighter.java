package AIF.AerialVehicles.Fighters;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class Fighter extends AerialVehicle {
    public Fighter(Coordinates location){
        super(location);
    }
}
