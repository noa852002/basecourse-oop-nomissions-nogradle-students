package AIF.AerialVehicles;


import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Gear.Gear;

import java.util.ArrayList;
import java.util.List;


public abstract class AerialVehicle {
    private int maintenanceMileage;
    private int mileageLeftUntilMaintenance;
    private int stations;
    private boolean isOnAir;
    private Coordinates location;
    private Coordinates destinationCoords;
    private final List<Gear> gearList;
    protected List<Class> gearsToLoad;


    public AerialVehicle(Coordinates location) {
        this.setMaintenanceMileage(Repository.maintenanceMileageList.get(this.getClass().getSuperclass()));
        this.setMileageLeftUntilMaintenance(this.getMaintenanceMileage());
        this.setStations(Repository.stationsList.get(this.getClass()));
        this.isOnAir = false;
        this.setLocation(location);
        this.gearList = new ArrayList<>();
        this.gearsToLoad = new ArrayList<>();
    }

    public void takeOff() throws CannotPerformOnGroundException, NeedMaintenanceException {
        if (!this.isOnAir() && !this.needMaintenance()) {
            System.out.println("Taking off...");
            this.setisOnAir(true);
        } else if (this.needMaintenance()){
            System.out.println("Vehicle needs maintenance...");
            throw new NeedMaintenanceException();
        }
        else {
            System.out.println("Plane can't take off while on air");
            throw new CannotPerformOnGroundException();
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformInMidAirException {
        if (this.isOnAir()) {
            this.destinationCoords = destination;
            System.out.println("Flying to " + destination.getLatitude() + "," + destination.getLongitude());
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isOnAir()) {
            System.out.println("Landing");
            this.setisOnAir(false);
            this.location = this.destinationCoords;
            double distance = this.getLocation().distance(this.getDestionationCoords());
            this.setMileageLeftUntilMaintenance(this.getMileageLeftUntilMaintenance() - (int) distance);
        } else {
            System.out.println("Plane can't land while not on air");
            throw new CannotPerformOnGroundException();
        }
    }

    public boolean needMaintenance() {
        return this.getMileageLeftUntilMaintenance() > this.maintenanceMileage;
    }

    public void performMaintenance() throws CannotPerformOnGroundException {
        if (!this.isOnAir()) {
            System.out.println("Performing maintenance");
            this.setMileageLeftUntilMaintenance(this.getMaintenanceMileage());
        } else {
            System.out.println("Plane can't be maintained while on air");
            throw new CannotPerformOnGroundException();
        }
    }

    public void activateModule(Class gearType, Coordinates location) throws NoModuleCanPerformException, ModuleNotFoundException {
        boolean moduleActivated = false;
        boolean inactiveModule = false;

        while (!moduleActivated) {
            for (Gear gear : this.getGearList()) {
                if (gear.getClass().equals(gearType) && !gear.wasActivated()) {
                    gear.activate(location);
                    moduleActivated = true;
                } else if (gear.getClass() == gearType && gear.wasActivated()) {
                    inactiveModule = true;
                }

                if (moduleActivated) {
                    break;
                }
            }

            if (!moduleActivated && inactiveModule) {
                throw new NoModuleCanPerformException();
            } else if (!moduleActivated) {
                throw new ModuleNotFoundException();
            }
        }
    }

    public void loadModule(Gear gear) throws NoModuleStationAvailableException, ModuleNotCompatibleException {
        boolean isGearLoaded = false;
        
        for (Class gearType : this.gearsToLoad) {
            if (gear.getClass().equals(gearType) && this.getStations() > 0) {
                this.setStations(this.getStations() - 1);
                this.addGear(gear);
                System.out.println(this.getGearName(gear) + " was successfully loaded!");
                isGearLoaded = true;
                break;
            } else if (this.getStations() == 0) {
                throw new NoModuleStationAvailableException();
            }
        }

        if (!isGearLoaded) {
            throw new ModuleNotCompatibleException();
        }
    }

    public Coordinates getLocation() {
        return location;
    }

    public void setLocation(Coordinates location) {
        this.location = location;
    }

    public Coordinates getDestionationCoords() {
        return this.destinationCoords;
    }

    public int getMaintenanceMileage() {
        return maintenanceMileage;
    }

    public void setMaintenanceMileage(int maintenanceMileage) {
        this.maintenanceMileage = maintenanceMileage;
    }

    public int getMileageLeftUntilMaintenance() {
        return this.mileageLeftUntilMaintenance;
    }

    public void setMileageLeftUntilMaintenance(int mileageLeftUntilMaintenance) {
        this.mileageLeftUntilMaintenance = mileageLeftUntilMaintenance;
    }

    public int getStations() {
        return stations;
    }

    public void setStations(int stations) {
        this.stations = stations;
    }

    public boolean isOnAir() {
        return this.isOnAir;
    }

    public void setisOnAir(boolean isOnAir) {
        this.isOnAir = isOnAir;
    }

    public List<Gear> getGearList() {
        return gearList;
    }

    public void addGear(Gear gearToLoad) {
        this.gearList.add(gearToLoad);
    }

    public String getGearName(Gear gear) {
        return gear.getClass().toString().split("\\.")[gear.getClass().toString().split("\\.").length - 1];
    }
}
