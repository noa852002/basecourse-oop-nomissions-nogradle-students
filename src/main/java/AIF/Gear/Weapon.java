package AIF.Gear;

public class Weapon extends Gear {
    private boolean wasActivated;

    public Weapon(){
        super(1500);
        this.wasActivated = false;
    }

    public boolean isWasActivated() {
        return this.wasActivated;
    }

    public void setWasActivated(boolean wasActivated) {
        this.wasActivated = wasActivated;
    }
}
