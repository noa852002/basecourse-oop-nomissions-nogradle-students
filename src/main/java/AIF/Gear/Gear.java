package AIF.Gear;

import AIF.Entities.Coordinates;

public abstract class Gear {
    private int actionRange;
    private boolean wasActivated;

    public Gear(int actionRange) {
        this.actionRange = actionRange;
        this.wasActivated = false;
    }

    public void activate(Coordinates location) {
        this.wasActivated = true;
        String gearName = this.getClass().toString().split("\\.")[this.getClass().toString().split("\\.").length - 1];
        System.out.println(gearName + " successfully activated in location " + location.getLatitude() + "," + location.getLongitude());
    }

    public int getActionRange() {
        return this.actionRange;
    }

    public void setActionRange(int actionRange) {
        this.actionRange = actionRange;
    }

    public boolean wasActivated() {
        return wasActivated;
    }

    public void setWasActivated(boolean wasActivated) {
        this.wasActivated = wasActivated;
    }
}
