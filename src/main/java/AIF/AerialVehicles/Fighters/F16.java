package AIF.AerialVehicles.Fighters;


import AIF.Entities.Coordinates;
import AIF.Gear.BDA;
import AIF.Gear.Weapon;

public class F16 extends Fighter {
    public F16(Coordinates location) {
        super(location);
        this.gearsToLoad.add(Weapon.class);
        this.gearsToLoad.add(BDA.class);
    }
}
