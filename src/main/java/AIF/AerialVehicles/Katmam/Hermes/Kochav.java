package AIF.AerialVehicles.Katmam.Hermes;

import AIF.Entities.Coordinates;
import AIF.Gear.BDA;
import AIF.Gear.Sensor;
import AIF.Gear.Weapon;

public class Kochav extends Hermes{
    public Kochav(Coordinates location){
        super(location);
        this.gearsToLoad.add(Sensor.class);
        this.gearsToLoad.add(Weapon.class);
        this.gearsToLoad.add(BDA.class);
    }
}
