package AIF.AerialVehicles.Katmam.Hermes;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class Hermes extends AerialVehicle {
    public Hermes(Coordinates location){
        super(location);
    }
}
