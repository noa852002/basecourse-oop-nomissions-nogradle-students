package AIF.AerialVehicles.Katmam.Haron;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class Haron extends AerialVehicle {

    public Haron(Coordinates location) {
        super(location);
    }
}
