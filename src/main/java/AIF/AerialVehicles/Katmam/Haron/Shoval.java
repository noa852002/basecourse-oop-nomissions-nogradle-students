package AIF.AerialVehicles.Katmam.Haron;

import AIF.Entities.Coordinates;
import AIF.Gear.BDA;
import AIF.Gear.Sensor;
import AIF.Gear.Weapon;

public class Shoval extends Haron {
    public Shoval(Coordinates location) {
        super(location);
        this.gearsToLoad.add(BDA.class);
        this.gearsToLoad.add(Weapon.class);
        this.gearsToLoad.add(Sensor.class);
    }
}

