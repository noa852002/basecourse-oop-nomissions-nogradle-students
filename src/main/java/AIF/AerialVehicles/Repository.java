package AIF.AerialVehicles;

import AIF.AerialVehicles.Fighters.F15;
import AIF.AerialVehicles.Fighters.F16;
import AIF.AerialVehicles.Fighters.Fighter;
import AIF.AerialVehicles.Katmam.Haron.Eitan;
import AIF.AerialVehicles.Katmam.Haron.Haron;
import AIF.AerialVehicles.Katmam.Haron.Shoval;
import AIF.AerialVehicles.Katmam.Hermes.Hermes;
import AIF.AerialVehicles.Katmam.Hermes.Kochav;
import AIF.AerialVehicles.Katmam.Hermes.Zik;

import java.util.HashMap;

public class Repository {
    final static HashMap<Class, Integer> stationsList = new HashMap<>() {{
        put(Kochav.class, 5);
        put(Zik.class, 1);
        put(Eitan.class, 4);
        put(Shoval.class, 3);
        put(F16.class, 7);
        put(F15.class, 10);
    }};

    final static HashMap<Class, Integer> maintenanceMileageList = new HashMap<>() {{
        put(Haron.class, 15000);
        put(Hermes.class, 10000);
        put(Fighter.class, 25000);
    }};
}
