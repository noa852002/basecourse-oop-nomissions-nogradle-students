package AIF.AerialVehicles.Katmam.Hermes;

import AIF.Entities.Coordinates;
import AIF.Gear.BDA;
import AIF.Gear.Sensor;

public class Zik extends Hermes {
    public Zik(Coordinates location) {
        super(location);
        this.gearsToLoad.add(BDA.class);
        this.gearsToLoad.add(Sensor.class);
    }
}