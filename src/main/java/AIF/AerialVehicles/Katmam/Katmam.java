package AIF.AerialVehicles.Katmam;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class Katmam extends AerialVehicle {

    public Katmam(int maintenanceMileage, Coordinates location){
        super(location);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if (this.isOnAir()) {
            System.out.println("Hovering over " + this.getLocation().toString());
            this.setMileageLeftUntilMaintenance((int) hours * 150 + this.getMileageLeftUntilMaintenance());
        } else {
            System.out.println("Plane can't hover while not on air");
            throw new CannotPerformOnGroundException();
        }
    }
}
