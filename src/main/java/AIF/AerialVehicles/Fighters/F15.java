package AIF.AerialVehicles.Fighters;
import AIF.Entities.Coordinates;
import AIF.Gear.Sensor;
import AIF.Gear.Weapon;

public class F15 extends Fighter {
    public F15(Coordinates location) {
        super(location);
        this.gearsToLoad.add(Weapon.class);
        this.gearsToLoad.add(Sensor.class);
    }
}
